print("Welcome to the investment doubler calculator")
print("\nYou will enter your initial investment and interest rate,")
print("and we'll calculate how long it will take to double your money!")

investment_start = input("\nHow much in US Dollars will be your initial investment? ")
an_per_rate = input("\nWhat is the annual percentage rate? ")

apr_as_decimal = float(an_per_rate) * 0.01

yearlygrowth = (float(investment_start) * float(apr_as_decimal)) + float(investment_start)
totalyears = 1


while float(yearlygrowth) < float(investment_start) * 2:
     print(f"At the end of year {totalyears} your total ammount will be ${format(yearlygrowth, '.2f')}.")
     yearlygrowth = (float(yearlygrowth) * float(apr_as_decimal)) + float(yearlygrowth)
     totalyears += 1

print(f"\nIt will take {totalyears} years to double your money to ${format(yearlygrowth, '.2f')}.")

