#Week 6, Assignment 6.1
"""
This week we will work with functions.
For this week’s assignment, write a program
hat uses a function to convert miles to kilometers.
Your program should prompt the user for the number of
miles driven, then call a function which converts
miles to kilometers. Check and validate all user
input and incorporate Try/Except block(s).
The program should then display the total miles and the kilometers.
"""
#Convert Miles to KM
def miles_to_km(miles):
    totalkm=float(miles)*1.609344
    print(f"\nYou entered {miles} miles, which converts to {format(totalkm, '.2f')} km. \n")


#Convert KM to Miles
def km_to_miles(km):
    totalmiles=float(km)/1.609344
    print(f"\nYou entered {km} km, which converts to {format(totalmiles, '.2f')} miles. \n")


#Confirm valid input type
def checkinput(mes_type):
    if mes_type == "miles":
        x = 0
    elif mes_type == "km":
        x = 0
    else:
        x = 1
    try:
        z = 5/0
    except:
        print("You must select 'miles' or 'km'. \n")


# Welcome
print("Welcome to the distance converter.")

# Prompt for km to miles or miles to km
mes_type = input("What would you like to convert? (miles/km)? ")

# Check input type with function
checkinput(mes_type)

# Do calculation via functions
if mes_type == "miles":
    miles = input("How many miles do you need to convert? ")
    miles_to_km(miles)
elif mes_type == "km":
    km = input("How many km do you need to convert? ")
    km_to_miles(km)
