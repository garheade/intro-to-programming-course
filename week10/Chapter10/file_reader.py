with open('pi_digits.txt') as testobj:
    contents=testobj.read()
print(contents.rstrip())

#####

filename = 'pi_digits.txt'

with open(filename) as file_object:
    for line in file_object:
        print(line.rstrip())


#####

filename = 'pi_digits.txt'

with open(filename) as fileobj:
    lines=fileobj.readlines()

for line in lines:
    print(line.rstrip())

#####

filename = 'pi_million_digits.txt'

with open(filename) as fileobj:
    lines=fileobj.readlines()

pi_string = ''

for line in lines:
    pi_string += line.strip()

print(f"{pi_string[:52]}...")
print(len(pi_string))


#####

filename = 'pi_million_digits.txt'

with open(filename) as fileobj:
    lines=fileobj.readlines()

pi_string = ''

for line in lines:
    pi_string += line.strip()

birthday = input("Enter your birthday, in the form mmddyy: ")
if birthday in pi_string:
    print("Your birthday appears in the first million digits of pi!")
else:
    print("Your birthday does not appear in the first million digits of pi.")

#####

filename = 'programming.txt'

try:
    file = open(filename, 'r')
except IOError:
    file = open(filename, 'w')

with open(filename, 'r+') as file_object:
    file_object.write("I Love Programming")
