"""
This week we will work with dictionaries.
Create a program that includes a dictionary of stocks.
Your dictionary should include at least 10 ticker symbols.
The key should be the stock ticker symbol and
the value should be the current price of the stock
(the values can be fictional). Ask the user to enter a ticker symbol.
Your program will search the dictionary for the ticker symbol
and then print the ticker symbol and the stock price.
If the ticker symbol isn’t located, print a message
indicating that the ticker symbol wasn’t found
"""

# Create Dictionary (Using Crytpo values instead of stock values)

dictCrypt = {'BTC':'20', 'ETH':'5.50', 'BNB':'1.156', 'SOL':'86.98', 'DOGE': '0.13', 'ATOM':'18.29','FIL':'14.61','EOS':'2.06', 'KLAY':'0.74', 'OKB':'17.86'}

def choiceValidation(choice):
    if choice not in dictCrypt.keys():
        againYN = input("Your choice is not in our database. Please restart the program. ")
        exit()



#Welcome message
print("Welcome to the Crypto Coin Value List! \n")
print("Please see the list of Coins available and chose the one you would like to see the value of:\n")
for coin, value in dictCrypt.items():
    print(coin)
    print()

choice = input("What coin would you like the value of? ")

choiceValidation(choice)

disp_choice = dictCrypt[choice]

print(f"The value of {choice} is ${disp_choice}")
