# Assignment 8.1 ITP Week 8: Virtual Garage
#Global Variables:
isexit = 0
firstveh = 0
vehiclelist = []
options = []
# Define class and subclasses
class Vehicle:
    # Base vehicle attributes
    def __init__(self):
        self.veh_options = {'make': '', 'model': '', 'year': '', 'options': ''}

    def setMake(self):
        self.veh_options['make'] = make

    def setModel(self):
        self.veh_options['model'] = model

    def setYear(self):
        self.veh_options['year'] = year

    def setOptions(self):
        self.veh_options['options'] = options

    def displayOptions(self):
        print("")
        print(f"You successfully created a vehicle and here are the options you entered:")
        for x, y in self.veh_options.items():
            if isinstance(y, list):
                for z in y:
                    print(f"Optional item: {z}")
            else:
                print(f"The {x} is: {y}")

class Car(Vehicle):
    # Vehicle Class + Car attributes
    def __init__(self):
        super().__init__()
        self.veh_options = {'make': '', 'model': '', 'year': '', 'doors': ''}

    def setDoors(self):
        self.veh_options['doors'] = doors

    def displayOptions(self):
        print("")
        print(f"You successfully created a car and here are the options you entered:")
        for x, y in self.veh_options.items():
            if isinstance(y, list):
                for z in y:
                    print(f"Optional item: {z}")
            else:
                if x == 'doors':
                    print(f"The number of {x} is: {y}")
                else:
                    print(f"The {x} is: {y}")

class Truck(Vehicle):
    # Vehicle Class + Truck attributes
    def __init__(self):
        super().__init__()
        self.veh_options = {'make': '', 'model': '', 'year': '', 'bed': ''}

    def setBed(self):
        self.veh_options['bed'] = bed

    def displayOptions(self):
        print("")
        print(f"You successfully created a truck and here are the options you entered:")
        for x, y in self.veh_options.items():
            if isinstance(y, list):
                for z in y:
                    print(f"Optional item: {z}")
            else:
                if x == 'bed':
                    print(f"The {x} length is: {y}")
                else:
                    print(f"The {x} is: {y}")

# universal functions:
def displaycar():
    # Set entries for cars and append to list
    veh = Car()
    veh.setMake()
    veh.setModel()
    veh.setYear()
    veh.setOptions()
    veh.setDoors()
    veh.displayOptions()
    vehiclelist.append(veh.veh_options)


def displaytruck():
    # Set entries for trucks and append to list
    veh = Truck()
    veh.setMake()
    veh.setModel()
    veh.setYear()
    veh.setBed()
    veh.setOptions()
    veh.displayOptions()
    vehiclelist.append(veh.veh_options)


def checkinput(veh_type):
    # Confirm valid menu choice
    if veh_type < 1 or veh_type > 4:
        raise ValueError("Invalid Response")

def listveh():
    # Display all vehicles entered
    count = 1
    for z in vehiclelist:
        print(f"\n\nVehicle {count} is: ")
        for x, y in z.items():
            if x == 'bed':
                print(f"The {x} length is: {y}")
            elif x == 'doors':
                print(f"The number of {x} is: {y}")
            elif isinstance(y, list):
                for z in y:
                    print(f"Optional item: {z}")
            else:
                print(f"The {x} is: {y}")

        count +=1
    print("\nNo More Vehicles\n")


# program

print("Welcome to the Romero Garage: \n")
print("Lets learn a little more about your vehicle. \n")
while isexit == 0:
    veh_type = input("Enter '1' for a car, '2' for a truck, '3' to list current vehicles, or '4' to quit: ")
    veh_type = int(veh_type)
    print("")
    checkinput(veh_type)

    if veh_type == 4:
        isexit = 1
        input("Press Enter to see final list and exit.")
    elif veh_type == 3:
        if firstveh == 0:
            raise Exception("No Vehicles in List")
        else:
            listveh()
    else:
        firstveh = 1
        make = input("What is the make of your vehicle? ")
        model = input("What is the model of your vehicle? ")
        year = input("What is the year of your vehicle? ")
        skip = input("Care to list an optional piece of equipment? (press Enter to skip): ")
        while skip is not "":
            options.append(skip)
            skip = input("Add another option? (press Enter when done): ")


    if veh_type == 2:
        bed = input("What is the length of the bed? ")
        displaytruck()
        print("")
    elif veh_type == 1:
        doors = input("What is the number of doors? ")
        displaycar()
        print("")

listveh()
exit()
