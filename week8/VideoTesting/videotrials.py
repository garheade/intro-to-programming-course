class employee:

    def __init__(self, first, last, pay):
        self.first = first
        self.last = last
        self.pay = pay
        self.email = first + '.' + last + '@blah.com'

    def fullname(self):
        return '{} {}'.format(self.first, self.last)


emp_1 = employee('Corey', 'Schafer', 50000)
emp_2 = employee('Test', 'User', 65000)

employee.fullname(emp_1)
