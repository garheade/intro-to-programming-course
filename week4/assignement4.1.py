# Start program:

print("Welcome to the Cost Calculator!\n\n")

comp_name = input("What is the company name for this quote? ")

fib_footage = input("\nWhat is the footage required for this firber optic line? (in feet): ")


if float(fib_footage) <= 250 and float(fib_footage) > 100:
    set_price = 0.80
    discount_yn = 'yes'
elif float(fib_footage) <= 500 and float(fib_footage) > 250:
    set_price = 0.70
    discount_yn = 'yes'
elif float(fib_footage) > 500:
    set_price = 0.50
    discount_yn = 'yes'
else:
    set_price = 0.87
    discount_yn = 'no'

orig_cost = float(fib_footage) * 0.87
disc_cost = float(fib_footage) * float(set_price)

if discount_yn == 'yes':
    print(f"The estimated cost for {comp_name.title()} is \n\n${format(orig_cost, '.2f')}.")
    print(f"\n\nBecause of the size of the job we can offer {comp_name.title()} a discounted price of \n\n${format(disc_cost, '.2f')}.")
else:
    print(f"The estimated cost for {comp_name.title()} is ${format(orig_cost, '.2f')}.")

print("\nThank you.\n")
