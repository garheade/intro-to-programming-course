# create list of trees
trees = ['Pine', 'Oak', 'Elm', 'Sycamore']

#print a couple trees
print(trees[2])
print(trees[3])

#include IF/ELSE statement
tree_num = input("What Tree Number?")
tree_num = int(tree_num) -1
if int(tree_num) > len(trees):
     print("Incorrect Tree")
else:
     print(trees[int(tree_num)])
