# Weather App for ITP Final Project
# Author: Gary Romero
# Creation Date: 05/11/2022
"""
For your class project we will be creating an application to interacts with a webservice in order to obtain data.  Your program will use all of the information you’ve learned in the class in order to create a useful application.
Your program must prompt the user for their city or zip code and request weather forecast data from openweathermap.org.  Your program must display the weather information in an READABLE format to the user.
Requirements:

    Create a Python Application which asks the user for their zip code or city.
    Use the zip code or city name in order to obtain weather forecast data from: http://openweathermap.org/
    Display the weather forecast in a readable format to the user.
    Use comments within the application where appropriate in order to document what the program is doing.
    Use functions including a main function.
    Allow the user to run the program multiple times.
    Validate whether the user entered valid data.  If valid data isn’t presented notify the user.
    Use the Requests library in order to request data from the webservice.
    Use Python 3.
    Use try blocks when establishing connections to the webservice. You must print a message to the user indicating whether or not the connection was successful.
"""

#imports
import datetime
import requests
import json
import ast

#variables
runagain = 'yes'
locationtype = ''
location = ''
lat = ''
lon = ''
now = datetime.datetime.now()
apikey = "&appid=2155936c6fea28b1bad56fd2e5a9a319"

class CurrentWeather:
    # Creating weather object
    def __init__(self):
        self.weth_attr = {'day': '', 'temp': '', 'feels_like': '', 'description': ''}

    def setDay(self):
        self.weth_attr['day'] = day

    def setTemp(self):
        self.weth_attr['temp'] = temp

    def setFL(self):
        self.weth_attr['feels_like'] = fl

    def setDesc(self):
        self.weth_attr['description'] = description



# functions:
def testjson(geolocation):
    #test if there is a payload from api call
    if geolocation.json() == "":
        print("Location returned no response, please check location and try again.")


def introDisplay():
    # Introduction Display Message
    print("Welcome to the Weather Application")
    print(f"Today' is {now.strftime('%A, %B the %dth, %Y')}\n")

def locationReq():
    # Ask user for location of weather and get lat/long from api
    global location, lat, lon
    location = input("What location would you like to check the weather for? (City Name or Zip Code): ")
    try:
        a = int(location)
        locationtype = 'zip'
        geolocation = requests.get("https://api.openweathermap.org/geo/1.0/zip?zip=" + location + ",us&" + apikey)
        try:
            lat = geolocation.json()["lat"]
            lon = geolocation.json()["lon"]
            print("\nvalid location\n")
        except:
            print("This zip code is invalid.")
    except:
        locationtype = 'city'
        state = input("Please enter a 2 digit state code? (i.e. NY, MN, CO...): ")
        geolocation = requests.get("https://api.openweathermap.org/geo/1.0/direct?q=" + location + "," + state + ",us&" + apikey)
        try:
            lat = geolocation.json()[0]["lat"]
            lon = geolocation.json()[0]["lon"]
            print("\nvalid location\n")
        except:
            print("This City/State is invalid.")


def getWeather():
    # Make API Call from openweathermap'
    global apicall
    apicall = requests.get("https://api.openweathermap.org/data/2.5/onecall?units=imperial&lat=" + str(lat) + "&lon=" + str(lon) + "&appid=2155936c6fea28b1bad56fd2e5a9a319")
    a = int(apicall.status_code)
    try:
        a = 200
        testjson(apicall)

    except:
        print("No values returned. Please check Zip Code or City/State and try again.")

def displayWeather():
    # Setting and Displaying Weather Info
    # Raise variables
    global apicall, temp, day, fl, description
    #Convert json
    currentDict = apicall.json()['current']
    weatherDict = str(currentDict['weather']).strip("[]")
    weatherDict = ast.literal_eval(weatherDict)
    #Set Variables from json output
    temp = currentDict.get('temp')
    day = weatherDict.get('main')
    fl = currentDict.get('feels_like')
    description = weatherDict.get('description')
    #Call function for object
    wet = CurrentWeather()
    wet.setDay()
    wet.setTemp()
    wet.setFL()
    wet.setDesc()
    #Print Weather info
    print(f"The day is {wet.weth_attr['day']}")
    print(f"with {wet.weth_attr['description']}")
    print(f"The current temperature is {wet.weth_attr['temp']}F")
    print(f"but feels like {wet.weth_attr['feels_like']}\n")

    print()
    print()




#Main Program

introDisplay()

while runagain == 'yes' or runagain =='y' or runagain == '':
    locationReq()
    getWeather()
    displayWeather()
    runagain = (input("Would you like to check another location? (yes/no)"))
    if runagain == 'no':
        print("Thank you for using our Weather App!")
