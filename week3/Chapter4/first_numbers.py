# List numbers from 1 to 4
for value in range(1, 5):
    print(value)

# List numbers from 1 to 5
for value in range(1, 6):
    print(value)

# make a list of numbers
numbers = list(range(1, 6))
print(numbers)
