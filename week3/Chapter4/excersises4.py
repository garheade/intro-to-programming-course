# 4-1 List Pizzas and print a statement about them
pizzas = ['cheese','pepperoni','supreme','veggie lovers']
for pizza in pizzas:
    print(f"My favorite pizza is {pizza}.")
print("\nI really love pizza\n")


# 4-2 Animals
animals = ['horse','cat','dog','fish']
for animal in animals:
    print(f"\nA {animal} would make a great pet!\n")
print("\nYou shouldn't eat any of these animals\n")

# 4-3 Count to twenty
num_count = list(range(1,21))
print(num_count)

# 4-4 list of one million
millions = list(range(1, 1000001))
for number in millions:
    print(number)

# 4-5 Sum one million
millions = list(range(1, 1000001))
total_sum = sum(millions)
print(total_sum)

# 4-6 Odd numbers
odd_num = list(range(1, 21, 2))
for num in odd_num:
    print(num)

# 4-7 Threes
threes = list(range(3, 31, 3))
for three in threes:
    print(three)

#4-8 cubes
cubes = [value**3 for value in range(1,10)]
for cube in cubes:
    print(cube)

# 4-9 Cube comprehension - see 4-8
