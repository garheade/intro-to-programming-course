squares = [] # create empty list
for value in range(1, 11): #for loop with range 1-10
    square = value ** 2 #calculate the sqare of each number
    squares.append(square) #append the value of square to the squares list
print(squares) # print the list

# Cleaner version:
squares = [] # create empty list
for value in range(1, 11): #for loop with range 1-10
    squares.append(value ** 2) #append the value of square to the squares list
print(squares) # print the list

# List Comprehension for square
squares = [value**2 for value in range (1,11)]
print(squares)
