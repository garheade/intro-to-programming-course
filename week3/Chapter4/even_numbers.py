# Create a list of even numbers
even_numbers = list(range(2, 11, 2)) # (2, 11, 2) tells python to create a range 2-11 and increment by the final 2
print(even_numbers)
