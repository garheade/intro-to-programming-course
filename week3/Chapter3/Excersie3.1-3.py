# Exc 3-1

names = ['gary', 'greg', 'kyle', 'kaitlyn', 'mackenzie']
name_position = ['first', 'second', 'third', 'fourth', 'fifth']

name_len = len(names)

for e in range(name_len):
  print("The ", name_position[e], "name on the list is ", names[e].title())
#  print(name_len)

# Exc 3-2
for e in range(name_len):
    print(f"Hello {names[e].title()}, how are you today?")

# Exc 3-3
armies = ['Skorne', 'Legion', 'Infernals']

armies_len = len(armies)
for e in range(armies_len):
    print(f"I'd love to own a {armies[e].title()} army.")
