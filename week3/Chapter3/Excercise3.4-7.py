print("\nExcercise 3-4 Guest List\n")

# Create Guest List variable
guestlist = ['orson scott card', 'david brin', 'linus torvald']

# Count Number of Guests for for loop
len_guest = len(guestlist)

# Loop through names
for e in range(len_guest):
    print(f"Hello, {guestlist[e].title()}, would you like to join me for dinner?")

print("\nExcercise 3-5 Changing Guest list\n")

# Reassigne guestlist variable
guestlist = ['orson scott card', 'david brin', 'linus torvald']

# count number of guests in list for for loop
len_guest = len(guestlist)

# Loop through names
for e in range(len_guest):
    print(f"Hello, {guestlist[e].title()}, would you like to join me for dinner?")

# Remove david brin from list.
pop_guest = guestlist.remove('david brin')
# Replace with john scalzi
guestlist.append('john scalzi')

print(f"{pop_guest.title()} can't make it, lets invite John Scalzi instead")

# loop through new list of Guests
for e in range(len_guest):
    print(f"Hello, {guestlist[e].title()}, would you like to join me for dinner?")

print("\nExcercise 3-6\n")

# for loop to let people know were inviting more people
for e in range(len_guest):
    print(f"Hey, {guestlist[e].title()}, I found a bigger table. Lets invite more people!")
# list of new Guests
newguests = ['stephen king', 'm. night shamalan', 'peter v. brett']
#modify list to add new guest to guestlist
guestlist.insert(0, newguests[0])
guestlist.insert(2, newguests[1])
guestlist.append(newguests[2])
#recount of guestlist variable
len_guest = len(guestlist)
#loop through updated list and invite all guests
for e in range(len_guest):
    print(f"Hello, {guestlist[e].title()}, would you like to join me for dinner?")

print("\nExcercise 3-7 Shrining Guest List\n")
# repeat of loop to invite guests.
for e in range(len_guest):
    print(f"Hello, {guestlist[e].title()}, would you like to join me for dinner?")

# set i to len_guest value for while loop
i = len_guest
#while loop to cancel some guests.
while i > 2:
    print(f"I'm sorry, {guestlist.pop().title()}, but we have to cancel dinner.")
    i = i - 1
#reinvite the remaining two guests
for e in range(i):
    print(f"Hello, {guestlist[e].title()}, would you still like to join me for dinner?")
#clear list with del command in for loop
for e in range(i):
    del guestlist[0]
    print(guestlist)
