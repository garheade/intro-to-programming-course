#api tests

import requests
import datetime
import json

unixfields = ("dt", "sunrise", "sunset", "moonrise", "moonset" )

# coordinates = requests.get("https://api.openweathermap.org/geo/1.0/zip?zip=80128,us&appid=2155936c6fea28b1bad56fd2e5a9a319")
coordinates = requests.get("https://api.openweathermap.org/geo/1.0/direct?q=denver,us&appid=2155936c6fea28b1bad56fd2e5a9a319")

print(coordinates.status_code)
lat = coordinates.json()[0]["lat"]
lon = coordinates.json()[0]["lon"]

print("https://api.openweathermap.org/data/2.5/onecall?lat=" + str(lat) + "&lon=" + str(lon) + "&appid=2155936c6fea28b1bad56fd2e5a9a319")

apicall = requests.get("https://api.openweathermap.org/data/2.5/onecall?units=imperial&lat=" + str(lat) + "&lon=" + str(lon) + "&appid=2155936c6fea28b1bad56fd2e5a9a319")

apioutput = apicall.json()
print(apicall.json()['current']['temp'])

print(apicall.status_code)
for key, value in apioutput.items():
    print(key, ":", value)
