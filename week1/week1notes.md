Chose one of these questions for the initial discussion posting. A minimum of three responses required.


* Discussion 1
        Look for several job postings related to software engineering and software development especially jobs specifically related to Python.  What are some of the skills required by companies for these roles?
* Discussion 2
        Conduct an internet search for flowchart software.  Evaluate several of the offerings available and provide an overview including; cost, functionality, ease of use, etc.
* Discussion 3
        Conduct an internet search for Python.  Evaluate several resources that you believe might be useful in assisting you throughout this course.  Provide a description on the resource and why it is useful.

https://docs.python.org/3.8/tutorial/index.html

Python.org's tutorial is a fantastic resource for beggining with Python. Honestly, the whole python.org website is full of great resources.

https://docs.microsoft.com/en-us/windows/python/beginners

I'm not a fan of Windows, or Microsoft in general, but I know lots of people use it. The page linked above is a great place to start with. It covers installing Python and VS Code on Windows and then contains links on some really great tutorials and resources.
